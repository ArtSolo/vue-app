export default {
    state: {
        myState: [1, 2, 3]
    },
    getters: {
        getMyState: (state) => state.myState
    }
}